module go.jolheiser.com/gitea-webhook

go 1.17

require code.gitea.io/sdk/gitea v0.15.1

require github.com/hashicorp/go-version v1.6.0 // indirect
