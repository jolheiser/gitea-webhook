package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
)

func web() {
	http.HandleFunc("/", capture)
	fmt.Println("capture started")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}

func capture(_ http.ResponseWriter, r *http.Request) {
	hookSig := r.Header.Get("X-Gitea-Signature")

	payload, err := io.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()

	sig := hmac.New(sha256.New, []byte(secret))
	if _, err := sig.Write(payload); err != nil {
		panic(err)
	}
	payloadSig := hex.EncodeToString(sig.Sum(nil))

	if hookSig != payloadSig {
		panic("signatures do not match")
	}

	if err := os.MkdirAll("events", os.ModePerm); err != nil {
		panic(err)
	}

	jsonFile := r.Header.Get("X-Gitea-Event-Type") + ".json"
	fmt.Println(jsonFile)
	if err := os.WriteFile(filepath.Join("events", jsonFile), payload, os.ModePerm); err != nil {
		panic(err)
	}
}
