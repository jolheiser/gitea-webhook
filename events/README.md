# Events

This directory is examples payloads from Gitea webhooks.

More information can be found in the [official documentation](https://docs.gitea.io/en-us/webhooks/).

The name of the file is the event name, as found with the header `X-Gitea-Event-Type` (Gitea 1.16+).

The generator runs against http://try.gitea.io in order to get the most up-to-date examples.