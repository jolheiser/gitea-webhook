# Gitea Webhook

Webhook structs for Gitea webhook payloads.

[Example payloads](events)

[Payload generation timeline](TIMELINE.md)

## Generating

There are three required env variables

|Variable|Reason|
|---|---|
|`GITEA_TOKEN`|Gitea token for "main" account to use|
|`GITEA_TOKEN_ALT`|Gitea token for "alt" account (fork, PR review)|
|`WEBHOOK_URL`|URL that _this_ app will be running on|

## License

[MIT](LICENSE)