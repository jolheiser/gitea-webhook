package main

func main() {
	go web()
	ch := make(chan struct{}, 1)
	go generate(ch)
	<-ch
}
