package main

import (
	"encoding/base64"
	"fmt"
	"os"
	"time"

	"code.gitea.io/sdk/gitea"
)

const (
	endpoint = "https://try.gitea.io"
	secret   = "giteaSecret"
)

func generate(ch chan struct{}) {
	defer func() {
		ch <- struct{}{}
	}()

	token := os.Getenv("GITEA_TOKEN")
	if token == "" {
		fmt.Println("$GITEA_TOKEN required")
		return
	}

	tokenAlt := os.Getenv("GITEA_TOKEN_ALT")
	if tokenAlt == "" {
		fmt.Println("$GITEA_TOKEN_ALT required")
		return
	}

	webhookURL := os.Getenv("WEBHOOK_URL")
	if webhookURL == "" {
		fmt.Println("$WEBHOOK_URL required")
		return
	}

	client, err := gitea.NewClient(endpoint, gitea.SetToken(token))
	if err != nil {
		fmt.Println("could not create client")
		panic(err)
	}

	clientAlt, err := gitea.NewClient(endpoint, gitea.SetToken(tokenAlt))
	if err != nil {
		fmt.Println("could not create alt client")
		panic(err)
	}

	altUser, _, err := clientAlt.GetMyUserInfo()
	if err != nil {
		fmt.Println("could not get alt user info")
		panic(err)
	}

	// Create repo
	repo, _, err := client.CreateRepo(gitea.CreateRepoOption{
		Name:          "gitea-webhook",
		Private:       true,
		AutoInit:      true,
		Readme:        "Default",
		IssueLabels:   "Default",
		DefaultBranch: "main",
	})
	if err != nil {
		fmt.Println("could not create repo")
		panic(err)
	}
	// (defer) Delete repos
	defer func() {
		// Sleep to wait for webhooks to finish
		time.Sleep(time.Second * 5)
		if _, err := client.DeleteRepo(repo.Owner.UserName, repo.Name); err != nil {
			fmt.Println("could not delete test repo")
		}
		if _, err := clientAlt.DeleteRepo(altUser.UserName, repo.Name); err != nil {
			fmt.Println("could not delete alt test repo")
		}
	}()

	// Create webhook
	if _, _, err := client.CreateRepoHook(repo.Owner.UserName, repo.Name, gitea.CreateHookOption{
		Type: gitea.HookTypeGitea,
		Config: map[string]string{
			"content_type": "json",
			"url":          webhookURL,
			"secret":       secret,
		},
		Events: []string{"push", "create", "pull_request", "issues", "delete", "fork", "repository", "release"},
		Active: true,
	}); err != nil {
		fmt.Println("could not create webhook")
		panic(err)
	}

	milestone, _, err := client.CreateMilestone(repo.Owner.UserName, repo.Name, gitea.CreateMilestoneOption{
		Title:       "Milestone 1",
		Description: "Milestone Description",
		State:       gitea.StateOpen,
	})
	if err != nil {
		fmt.Println("could not create milestone")
		panic(err)
	}

	labels, _, err := client.ListRepoLabels(repo.Owner.UserName, repo.Name, gitea.ListLabelsOptions{})
	if err != nil {
		fmt.Println("could not list repo labels")
		panic(err)
	}
	if len(labels) == 0 {
		panic("no labels found")
	}

	if _, err := client.AddCollaborator(repo.Owner.UserName, repo.Name, altUser.UserName, gitea.AddCollaboratorOption{}); err != nil {
		fmt.Println("could not add collaborator")
		panic(err)
	}

	// Create
	if _, _, err := client.CreateBranch(repo.Owner.UserName, repo.Name, gitea.CreateBranchOption{
		BranchName:    "alt",
		OldBranchName: repo.DefaultBranch,
	}); err != nil {
		fmt.Println("could not create new branch")
		panic(err)
	}

	// Delete
	defer func() {
		if _, _, err := client.DeleteRepoBranch(repo.Owner.UserName, repo.Name, "main"); err != nil {
			fmt.Println("could not delete branch")
			panic(err)
		}
	}()

	// Create file for PR later
	if _, _, err := client.CreateFile(repo.Owner.UserName, repo.Name, "FILE.txt", gitea.CreateFileOptions{
		FileOptions: gitea.FileOptions{
			BranchName: "alt",
		},
		Content: base64.StdEncoding.EncodeToString([]byte("New file!")),
	}); err != nil {
		fmt.Println("could not create file")
		panic(err)
	}

	// Fork
	if _, _, err := clientAlt.CreateFork(repo.Owner.UserName, repo.Name, gitea.CreateForkOption{}); err != nil {
		fmt.Println("could not fork repo")
		panic(err)
	}

	// Release
	if _, _, err := client.CreateRelease(repo.Owner.UserName, repo.Name, gitea.CreateReleaseOption{
		TagName: "release",
		Target:  repo.DefaultBranch,
		Title:   "Release 1",
		Note:    "Release Notes",
	}); err != nil {
		fmt.Println("could not create release")
		panic(err)
	}

	// Issue, assign, label, milestone
	issue, _, err := client.CreateIssue(repo.Owner.UserName, repo.Name, gitea.CreateIssueOption{
		Title:     "Issue 1",
		Body:      "Issue Body",
		Assignees: []string{repo.Owner.UserName},
		Milestone: milestone.ID,
		Labels:    []int64{labels[0].ID},
	})
	if err != nil {
		fmt.Println("could not create issue")
		panic(err)
	}

	// Issue comment
	if _, _, err := client.CreateIssueComment(repo.Owner.UserName, repo.Name, issue.Index, gitea.CreateIssueCommentOption{
		Body: "Issue Comment",
	}); err != nil {
		fmt.Println("could not create issue comment")
		panic(err)
	}

	// Pull request, assign, label, milestone
	pr, _, err := client.CreatePullRequest(repo.Owner.UserName, repo.Name, gitea.CreatePullRequestOption{
		Head:      "alt",
		Base:      repo.DefaultBranch,
		Title:     "Pull Request 1",
		Body:      "Pull Request Body\nCloses #1",
		Assignee:  repo.Owner.UserName,
		Assignees: nil,
		Milestone: milestone.ID,
		Labels:    []int64{labels[0].ID},
	})
	if err != nil {
		fmt.Println("could not create PR")
		panic(err)
	}

	// PR comment
	if _, _, err := client.CreateIssueComment(repo.Owner.UserName, repo.Name, pr.Index, gitea.CreateIssueCommentOption{
		Body: "Pull Request Comment",
	}); err != nil {
		fmt.Println("could not create PR comment")
		panic(err)
	}

	// PR review reject
	if _, _, err := clientAlt.CreatePullReview(repo.Owner.UserName, repo.Name, pr.Index, gitea.CreatePullReviewOptions{
		State: gitea.ReviewStateRequestChanges,
		Body:  "Please make changes",
	}); err != nil {
		fmt.Println("could not reject PR")
		panic(err)
	}

	// PR review approve
	if _, _, err := clientAlt.CreatePullReview(repo.Owner.UserName, repo.Name, pr.Index, gitea.CreatePullReviewOptions{
		State: gitea.ReviewStateApproved,
		Body:  "LGTM",
	}); err != nil {
		fmt.Println("could not approve PR")
		panic(err)
	}

	// FIXME? Pull request review comments don't show up

	// PR sync
	prFile, _, err := client.GetContents(repo.Owner.UserName, repo.Name, "alt", "FILE.txt")
	if err != nil {
		fmt.Println("could not sync PR")
		panic(err)
	}
	if _, _, err := client.UpdateFile(repo.Owner.UserName, repo.Name, "FILE.txt", gitea.UpdateFileOptions{
		FileOptions: gitea.FileOptions{
			BranchName: "alt",
		},
		SHA:     prFile.SHA,
		Content: base64.StdEncoding.EncodeToString([]byte("New updated file!")),
	}); err != nil {
		fmt.Println("could not update file")
		panic(err)
	}

	// PR merge
	if _, _, err := client.MergePullRequest(repo.Owner.UserName, repo.Name, pr.Index, gitea.MergePullRequestOption{
		Style: gitea.MergeStyleMerge,
	}); err != nil {
		fmt.Println("could not merge PR")
		panic(err)
	}
}
